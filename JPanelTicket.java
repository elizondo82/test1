
package com.openbravo.pos.sales;

import bsh.EvalError;
import bsh.Interpreter;
import cm.itkamer.opencart.model.DataLogicIntegration;
import cm.itkamer.opencart.model.OpencartProductInfoExt;
import cm.itkamer.opencart.ws.OpencartWebServiceConsumer;
import com.openbravo.basic.BasicException;
import com.openbravo.beans.JNumberEvent;
import com.openbravo.beans.JNumberEventListener;
import com.openbravo.beans.JNumberKeys;
import com.openbravo.beans.ShortCutInfo;
import com.openbravo.data.gui.ComboBoxValModel;
import com.openbravo.data.gui.ListKeyed;
import com.openbravo.data.gui.MessageInf;
import com.openbravo.data.loader.SentenceList;
import com.openbravo.data.loader.Session;
import com.openbravo.pos.customers.CustomerInfo;
import com.openbravo.pos.customers.CustomerInfoExt;
import com.openbravo.pos.customers.DataLogicCustomers;
import com.openbravo.pos.customers.JCustomerFinder;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.AppProperties;
import com.openbravo.pos.forms.AppUser;
import com.openbravo.pos.forms.AppUserView;
import com.openbravo.pos.forms.AppView;
import com.openbravo.pos.forms.BeanFactoryApp;
import com.openbravo.pos.forms.BeanFactoryException;
import com.openbravo.pos.forms.DataLogicSales;
import com.openbravo.pos.forms.DataLogicSystem;
import com.openbravo.pos.forms.JPanelView;
import com.openbravo.pos.forms.JRootApp;
import com.openbravo.pos.inventory.TaxCategoryInfo;
import com.openbravo.pos.panels.JProductFinder;
import com.openbravo.pos.payment.JPaymentSelect;
import com.openbravo.pos.payment.JPaymentSelectReceipt;
import com.openbravo.pos.payment.JPaymentSelectRefund;
import com.openbravo.pos.printer.DeviceDisplay;
import com.openbravo.pos.printer.DeviceTicket;
import com.openbravo.pos.printer.TicketParser;
import com.openbravo.pos.printer.TicketPrinterException;
import com.openbravo.pos.sales.DataLogicReceipts;
import com.openbravo.pos.sales.JMooringDetails;
import com.openbravo.pos.sales.JPanelButtons;
import com.openbravo.pos.sales.JPanelTicket;
import com.openbravo.pos.sales.JProductAttEdit;
import com.openbravo.pos.sales.JProductLineEdit;
import com.openbravo.pos.sales.JTicketLines;
import com.openbravo.pos.sales.JTicketsBag;
import com.openbravo.pos.sales.KitchenDisplay;
import com.openbravo.pos.sales.ReceiptSplit;
import com.openbravo.pos.sales.TaxesException;
import com.openbravo.pos.sales.TaxesLogic;
import com.openbravo.pos.sales.TicketsEditor;
import com.openbravo.pos.sales.restaurant.RestaurantDBUtils;
import com.openbravo.pos.scale.DeviceScale;
import com.openbravo.pos.scale.ScaleException;
import com.openbravo.pos.scripting.ScriptEngine;
import com.openbravo.pos.scripting.ScriptException;
import com.openbravo.pos.scripting.ScriptFactory;
import com.openbravo.pos.ticket.DeferredTicketInfo;
import com.openbravo.pos.ticket.ProductInfoExt;
import com.openbravo.pos.ticket.TaxInfo;
import com.openbravo.pos.ticket.TicketInfo;
import com.openbravo.pos.ticket.TicketLineInfo;
import com.openbravo.pos.ticket.TicketTaxInfo;
import com.openbravo.pos.ticket.UserInfo;
import com.openbravo.pos.util.InactivityListener;
import com.openbravo.pos.util.JRPrinterAWT300;
import com.openbravo.pos.util.ReportUtils;
import com.openbravo.pos.vouchers.DataLogicVouchers;
import com.openbravo.pos.vouchers.VoucherPaymentInformation;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Stack;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.xml.rpc.ServiceException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.commons.lang.StringUtils;

public abstract class JPanelTicket
extends JPanel
implements JPanelView,
BeanFactoryApp,
TicketsEditor {
    private static final int NUMBERZERO = 0;
    private static final int NUMBERVALID = 1;
    private static final int NUMBER_INPUTZERO = 0;
    private static final int NUMBER_INPUTZERODEC = 1;
    private static final int NUMBER_INPUTINT = 2;
    private static final int NUMBER_INPUTDEC = 3;
    private static final int NUMBER_PORZERO = 4;
    private static final int NUMBER_PORZERODEC = 5;
    private static final int NUMBER_PORINT = 6;
    private static final int NUMBER_PORDEC = 7;
    private Stack<VoucherPaymentInformation> stackvouchers = new Stack();
    private VoucherPaymentInformation voucherpaymentinformation = new VoucherPaymentInformation();
    private DataLogicVouchers dlVouchers;
    protected JTicketLines m_ticketlines;
    private TicketParser m_TTP;
    protected TicketInfo m_oTicket;
    protected Object m_oTicketExt;
    private int m_iNumberStatus;
    private int m_iNumberStatusInput;
    private int m_iNumberStatusPor;
    private StringBuffer m_sBarcode;
    protected JTicketsBag m_ticketsbag;
    private SentenceList senttax;
    private ListKeyed taxcollection;
    private SentenceList senttaxcategories;
    private ListKeyed taxcategoriescollection;
    private ComboBoxValModel taxcategoriesmodel;
    private TaxesLogic taxeslogic;
    protected JPanelButtons m_jbtnconfig;
    protected AppView m_App;
    protected DataLogicSystem dlSystem;
    protected DataLogicSales dlSales;
    protected DataLogicCustomers dlCustomers;
    private JPaymentSelect paymentdialogreceipt;
    private JPaymentSelect paymentdialogrefund;
    private JRootApp root;
    private Object m_principalapp;
    private Boolean restaurant;
    private Action logout;
    private InactivityListener listener;
    private Integer delay = 0;
    private final String m_sCurrentTicket = null;
    protected TicketsEditor m_panelticket;
    private DataLogicReceipts dlReceipts = null;
    private Boolean priceWith00;
    private final String temp_jPrice = "";
    private String tableDetails;
    protected RestaurantDBUtils restDB;
    private KitchenDisplay kitchenDisplay;
    private String ticketPrintType;
    private Boolean warrantyPrint = false;
    private DataLogicIntegration dlIntegration;
    private JButton btnCustomer;
    private JButton btnSplit;
    private JPanel catcontainer;
    private JButton jButton1;
    private JButton jEditAttributes;
    private JLabel jLabel1;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel9;
    private JButton j_btnKitchenPrt;
    private JButton jbtnMooring;
    private JPanel m_jButtons;
    private JPanel m_jButtonsExt;
    private JPanel m_jContEntries;
    private JButton m_jDelete;
    private JButton m_jDown;
    private JButton m_jEditLine;
    private JButton m_jEnter;
    private JTextField m_jKeyFactory;
    private JLabel m_jLblTotalEuros1;
    private JLabel m_jLblTotalEuros2;
    private JLabel m_jLblTotalEuros3;
    private JButton m_jList;
    private JNumberKeys m_jNumberKeys;
    private JPanel m_jOptions;
    private JPanel m_jPanContainer;
    private JPanel m_jPanEntries;
    private JPanel m_jPanTicket;
    private JPanel m_jPanTotals;
    private JPanel m_jPanelBag;
    private JPanel m_jPanelCentral;
    private JPanel m_jPanelScripts;
    private JLabel m_jPor;
    private JLabel m_jPrice;
    private JLabel m_jSubtotalEuros;
    private JComboBox m_jTax;
    private JLabel m_jTaxesEuros;
    private JLabel m_jTicketId;
    private JLabel m_jTotalEuros;
    private JButton m_jUp;
    private JToggleButton m_jaddtax;
    private JButton m_jbtnScale;

    public JPanelTicket() {
        this.initComponents();
    }

    public void init(AppView app) throws BeanFactoryException {
        this.m_App = app;
        this.restDB = new RestaurantDBUtils(this.m_App);
        this.dlSystem = (DataLogicSystem)this.m_App.getBean("com.openbravo.pos.forms.DataLogicSystem");
        this.dlSales = (DataLogicSales)this.m_App.getBean("com.openbravo.pos.forms.DataLogicSales");
        this.dlCustomers = (DataLogicCustomers)this.m_App.getBean("com.openbravo.pos.customers.DataLogicCustomers");
        this.dlReceipts = (DataLogicReceipts)app.getBean("com.openbravo.pos.sales.DataLogicReceipts");
        this.dlVouchers = (DataLogicVouchers)this.m_App.getBean("com.openbravo.pos.vouchers.DataLogicVouchers");
        this.dlIntegration = (DataLogicIntegration)this.m_App.getBean("cm.itkamer.opencart.model.DataLogicIntegration");
        if (!this.m_App.getDeviceScale().existsScale()) {
            this.m_jbtnScale.setVisible(false);
        }
        if (Boolean.valueOf(this.m_App.getProperties().getProperty("till.amountattop")).booleanValue()) {
            this.m_jPanEntries.remove(this.jPanel9);
            this.m_jPanEntries.remove((Component)this.m_jNumberKeys);
            this.m_jPanEntries.add(this.jPanel9);
            this.m_jPanEntries.add((Component)this.m_jNumberKeys);
        }
        this.jbtnMooring.setVisible(Boolean.valueOf(this.m_App.getProperties().getProperty("till.marineoption")));
        this.priceWith00 = "true".equals(this.m_App.getProperties().getProperty("till.pricewith00"));
        if (this.priceWith00.booleanValue()) {
            this.m_jNumberKeys.dotIs00(true);
        }
        this.m_ticketsbag = this.getJTicketsBag();
        this.m_jPanelBag.add((Component)this.m_ticketsbag.getBagComponent(), "Before");
        this.add((Component)this.m_ticketsbag.getNullComponent(), "null");
        this.m_ticketlines = new JTicketLines(this.dlSystem.getResourceAsXML("Ticket.Line"), this);
        this.m_jPanelCentral.add((Component)this.m_ticketlines, "Center");
        this.m_TTP = new TicketParser(this.m_App.getDeviceTicket(), this.dlSystem);
        this.m_jbtnconfig = new JPanelButtons("Ticket.Buttons", this);
        this.m_jButtonsExt.add((Component)this.m_jbtnconfig);
        this.catcontainer.add(this.getSouthComponent(), "Center");
        this.catcontainer.add((Component)new ShortCutInfo(), "South");
        this.senttax = this.dlSales.getTaxList();
        this.senttaxcategories = this.dlSales.getTaxCategoriesList();
        this.taxcategoriesmodel = new ComboBoxValModel();
        this.stateToZero();
        this.m_oTicket = null;
        this.m_oTicketExt = null;
        this.jLabel1.setEnabled(false);
        this.jLabel1.setVisible(false);
    }

    public Object getBean() {
        return this;
    }

    public JComponent getComponent() {
        return this;
    }

    private void saveCurrentTicket() {
        String currentTicket = (String)this.m_oTicketExt;
        if (currentTicket != null) {
            try {
                this.dlReceipts.updateSharedTicket(currentTicket, this.m_oTicket, this.m_oTicket.getPickupId());
            }
            catch (BasicException e) {
                new MessageInf((Throwable)e).show((Component)this);
            }
        }
    }

    public void activate() throws BasicException {
        logout logout2 = new logout(this);
        String autoLogoff = this.m_App.getProperties().getProperty("till.autoLogoff");
        if (autoLogoff != null && autoLogoff.equals("true")) {
            try {
                this.delay = Integer.parseInt(this.m_App.getProperties().getProperty("till.autotimer"));
            }
            catch (NumberFormatException e) {
                this.delay = 0;
            }
            this.delay = this.delay * 1000;
        }
        if (this.delay != 0) {
            this.listener = new InactivityListener((Action)logout2, this.delay.intValue());
            this.listener.start();
        }
        this.paymentdialogreceipt = JPaymentSelectReceipt.getDialog((Component)this);
        this.paymentdialogreceipt.init(this.m_App);
        this.paymentdialogrefund = JPaymentSelectRefund.getDialog((Component)this);
        this.paymentdialogrefund.init(this.m_App);
        this.m_jaddtax.setSelected("true".equals(this.m_jbtnconfig.getProperty("taxesincluded")));
        List taxlist = this.senttax.list();
        this.taxcollection = new ListKeyed(taxlist);
        List taxcategorieslist = this.senttaxcategories.list();
        this.taxcategoriescollection = new ListKeyed(taxcategorieslist);
        this.taxcategoriesmodel = new ComboBoxValModel(taxcategorieslist);
        this.m_jTax.setModel(this.taxcategoriesmodel);
        String taxesid = this.m_jbtnconfig.getProperty("taxcategoryid");
        if (taxesid == null) {
            if (this.m_jTax.getItemCount() > 0) {
                this.m_jTax.setSelectedIndex(0);
            }
        } else {
            this.taxcategoriesmodel.setSelectedKey((Object)taxesid);
        }
        this.taxeslogic = new TaxesLogic(taxlist);
        this.m_jaddtax.setSelected(Boolean.parseBoolean(this.m_App.getProperties().getProperty("till.taxincluded")));
        if (this.m_App.getAppUserView().getUser().hasPermission("sales.ChangeTaxOptions")) {
            this.m_jTax.setVisible(true);
            this.m_jaddtax.setVisible(true);
        } else {
            this.m_jTax.setVisible(false);
            this.m_jaddtax.setVisible(false);
        }
        this.btnSplit.setEnabled(this.m_App.getAppUserView().getUser().hasPermission("sales.Total"));
        this.m_jDelete.setEnabled(this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines"));
        this.m_jNumberKeys.setMinusEnabled(this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines"));
        this.m_jNumberKeys.setEqualsEnabled(this.m_App.getAppUserView().getUser().hasPermission("sales.Total"));
        this.m_jbtnconfig.setPermissions(this.m_App.getAppUserView().getUser());
        this.m_ticketsbag.activate();
    }

    public boolean deactivate() {
        if (this.listener != null) {
            this.listener.stop();
        }
        return this.m_ticketsbag.deactivate();
    }

    protected abstract JTicketsBag getJTicketsBag();

    protected abstract Component getSouthComponent();

    protected abstract void resetSouthComponent();

    public void setActiveTicket(TicketInfo oTicket, Object oTicketExt) {
        switch (this.m_App.getProperties().getProperty("machine.ticketsbag")) {
            case "restaurant": {
                if (!"true".equals(this.m_App.getProperties().getProperty("till.autoLogoffrestaurant")) || this.listener == null) break;
                this.listener.restart();
            }
        }
        this.m_oTicket = oTicket;
        this.m_oTicketExt = oTicketExt;
        if (this.m_oTicket != null) {
            this.m_oTicket.setUser(this.m_App.getAppUserView().getUser().getUserInfo());
            this.m_oTicket.setActiveCash(this.m_App.getActiveCashIndex());
            this.m_oTicket.setDate(new Date());
            if ("restaurant".equals(this.m_App.getProperties().getProperty("machine.ticketsbag")) && !oTicket.getOldTicket()) {
                if (this.restDB.getCustomerNameInTable(oTicketExt.toString()) == null && this.m_oTicket.getCustomer() != null) {
                    this.restDB.setCustomerNameInTable(this.m_oTicket.getCustomer().toString(), oTicketExt.toString());
                }
                if (this.restDB.getWaiterNameInTable(oTicketExt.toString()) == null || "".equals(this.restDB.getWaiterNameInTable(oTicketExt.toString()))) {
                    this.restDB.setWaiterNameInTable(this.m_App.getAppUserView().getUser().getName(), oTicketExt.toString());
                }
                this.restDB.setTicketIdInTable(this.m_oTicket.getId(), oTicketExt.toString());
            }
        }
        if (this.m_oTicket == null || Boolean.parseBoolean(this.m_App.getProperties().getProperty("table.showwaiterdetails")) || Boolean.valueOf(this.m_App.getProperties().getProperty("table.showcustomerdetails")).booleanValue()) {
            // empty if block
        }
        if (this.m_oTicket != null && (Boolean.valueOf(this.m_App.getProperties().getProperty("table.showcustomerdetails")).booleanValue() || Boolean.parseBoolean(this.m_App.getProperties().getProperty("table.showwaiterdetails"))) && this.restDB.getTableMovedFlag(this.m_oTicket.getId()).booleanValue()) {
            this.restDB.moveCustomer(oTicketExt.toString(), this.m_oTicket.getId());
        }
        this.executeEvent(this.m_oTicket, this.m_oTicketExt, "ticket.show", new ScriptArg[0]);
        if (!"restaurant".equals(this.m_App.getProperties().getProperty("machine.ticketsbag"))) {
            this.j_btnKitchenPrt.setVisible(this.m_App.getAppUserView().getUser().hasPermission("sales.PrintKitchen"));
        }
        this.refreshTicket();
    }

    public TicketInfo getActiveTicket() {
        return this.m_oTicket;
    }

    protected void refreshTicket() {
        CardLayout cl = (CardLayout)this.getLayout();
        if (this.m_oTicket == null) {
            this.m_jTicketId.setText(null);
            this.m_ticketlines.clearTicketLines();
            this.m_jSubtotalEuros.setText(null);
            this.m_jTaxesEuros.setText(null);
            this.m_jTotalEuros.setText(null);
            this.jLabel1.setEnabled(false);
            this.stateToZero();
            this.repaint();
            cl.show(this, "null");
            if (this.m_oTicket != null && this.m_oTicket.getLinesCount() == 0) {
                this.resetSouthComponent();
            }
        } else {
            if (this.m_oTicket.getTicketType() == 1) {
                this.m_jEditLine.setVisible(false);
                this.m_jList.setVisible(false);
            }
            for (TicketLineInfo line : this.m_oTicket.getLines()) {
                line.setTaxInfo(this.taxeslogic.getTaxInfo(line.getProductTaxCategoryID(), this.m_oTicket.getCustomer()));
            }
            this.m_jTicketId.setText(this.m_oTicket.getName(this.m_oTicketExt));
            this.m_oTicket.getSumvoucher(this.dlVouchers, this.jLabel1);
            this.m_ticketlines.clearTicketLines();
            for (int i = 0; i < this.m_oTicket.getLinesCount(); ++i) {
                this.m_ticketlines.addTicketLine(this.m_oTicket.getLine(i));
            }
            this.printPartialTotals();
            this.stateToZero();
            cl.show(this, "ticket");
            if (this.m_oTicket.getLinesCount() == 0) {
                this.resetSouthComponent();
            }
            this.m_jKeyFactory.setText(null);
            EventQueue.invokeLater((Runnable)new /* Unavailable Anonymous Inner Class!! */);
        }
    }

    private void printPartialTotals() {
        if (this.m_oTicket.getLinesCount() == 0) {
            this.m_jSubtotalEuros.setText(null);
            this.m_jTaxesEuros.setText(null);
            this.m_jTotalEuros.setText(null);
            this.repaint();
        } else {
            this.m_jSubtotalEuros.setText(this.m_oTicket.printSubTotal());
            this.m_jTaxesEuros.setText(this.m_oTicket.printTax());
            this.m_jTotalEuros.setText(this.m_oTicket.printTotal());
        }
    }

    public void paintTicketLine(int index, TicketLineInfo oLine) {
        if (this.executeEventAndRefresh("ticket.setline", new ScriptArg("index", (Object)index), new ScriptArg("line", (Object)oLine)) == null) {
            this.m_oTicket.setLine(index, oLine);
            this.m_ticketlines.setTicketLine(index, oLine);
            this.m_ticketlines.setSelectedIndex(index);
            this.visorTicketLine(oLine);
            this.printPartialTotals();
            this.stateToZero();
            this.executeEventAndRefresh("ticket.change", new ScriptArg[0]);
        }
    }

    private void addTicketLine(ProductInfoExt oProduct, double dMul, double dPrice) {
        if (oProduct.isVprice()) {
            TaxInfo tax = this.taxeslogic.getTaxInfo(oProduct.getTaxCategoryID(), this.m_oTicket.getCustomer());
            if (this.m_jaddtax.isSelected()) {
                dPrice /= 1.0 + tax.getRate();
            }
            this.addTicketLine(new TicketLineInfo(oProduct, dMul, dPrice, tax, (Properties)oProduct.getProperties().clone()));
        } else {
            TaxInfo tax = this.taxeslogic.getTaxInfo(oProduct.getTaxCategoryID(), this.m_oTicket.getCustomer());
            this.addTicketLine(new TicketLineInfo(oProduct, dMul, dPrice, tax, (Properties)oProduct.getProperties().clone()));
        }
    }

    protected void addTicketLine(TicketLineInfo oLine) {
        if (this.executeEventAndRefresh("ticket.addline", new ScriptArg("line", (Object)oLine)) == null) {
            if (oLine.isProductCom()) {
                int i = this.m_ticketlines.getSelectedIndex();
                if (i >= 0 && !this.m_oTicket.getLine(i).isProductCom()) {
                    ++i;
                }
                while (i >= 0 && i < this.m_oTicket.getLinesCount() && this.m_oTicket.getLine(i).isProductCom()) {
                    ++i;
                }
                if (i >= 0) {
                    this.m_oTicket.insertLine(i, oLine);
                    this.m_ticketlines.insertTicketLine(i, oLine);
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            } else {
                this.m_oTicket.addLine(oLine);
                this.m_ticketlines.addTicketLine(oLine);
                try {
                    int i = this.m_ticketlines.getSelectedIndex();
                    TicketLineInfo line = this.m_oTicket.getLine(i);
                    if (line.isProductVerpatrib()) {
                        JProductAttEdit attedit = JProductAttEdit.getAttributesEditor((Component)this, (Session)this.m_App.getSession());
                        attedit.editAttributes(line.getProductAttSetId(), line.getProductAttSetInstId());
                        attedit.setVisible(true);
                        if (attedit.isOK()) {
                            line.setProductAttSetInstId(attedit.getAttributeSetInst());
                            line.setProductAttSetInstDesc(attedit.getAttributeSetInstDescription());
                            this.paintTicketLine(i, line);
                        }
                    }
                }
                catch (BasicException ex) {
                    MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotfindattributes"), (Object)ex);
                    msg.show((Component)this);
                }
            }
            this.visorTicketLine(oLine);
            this.printPartialTotals();
            this.stateToZero();
            this.executeEvent(this.m_oTicket, this.m_oTicketExt, "ticket.change", new ScriptArg[0]);
        }
    }

    private void removeTicketLine(int i) {
        if (this.executeEventAndRefresh("ticket.removeline", new ScriptArg("index", (Object)i)) == null) {
            String ticketID = Integer.toString(this.m_oTicket.getTicketId());
            if (this.m_oTicket.getTicketId() == 0) {
                ticketID = "No Sale";
            }
            this.dlSystem.execLineRemoved(new Object[]{this.m_App.getAppUserView().getUser().getName(), ticketID, this.m_oTicket.getLine(i).getProductID(), this.m_oTicket.getLine(i).getProductName(), this.m_oTicket.getLine(i).getMultiply()});
            if (this.m_oTicket.getLine(i).isProductCom()) {
                this.m_oTicket.removeLine(i);
                this.m_ticketlines.removeTicketLine(i);
            } else {
                this.m_oTicket.removeLine(i);
                this.m_ticketlines.removeTicketLine(i);
                while (i < this.m_oTicket.getLinesCount() && this.m_oTicket.getLine(i).isProductCom()) {
                    this.m_oTicket.removeLine(i);
                    this.m_ticketlines.removeTicketLine(i);
                }
            }
            this.visorTicketLine(null);
            this.printPartialTotals();
            this.stateToZero();
            this.executeEventAndRefresh("ticket.change", new ScriptArg[0]);
        }
    }

    private ProductInfoExt getInputProduct() {
        ProductInfoExt oProduct = new ProductInfoExt();
        oProduct.setID("xxx999_999xxx_x9x9x9");
        oProduct.setReference(null);
        oProduct.setCode(null);
        oProduct.setName("");
        oProduct.setTaxCategoryID(((TaxCategoryInfo)this.taxcategoriesmodel.getSelectedItem()).getID());
        oProduct.setPriceSell(this.includeTaxes(oProduct.getTaxCategoryID(), this.getInputValue()));
        return oProduct;
    }

    private double includeTaxes(String tcid, double dValue) {
        if (this.m_jaddtax.isSelected()) {
            TaxInfo tax = this.taxeslogic.getTaxInfo(tcid, this.m_oTicket.getCustomer());
            double dTaxRate = tax == null ? 0.0 : tax.getRate();
            return dValue / (1.0 + dTaxRate);
            }else{
               return dValue; 
            }
        }
    }

    private double excludeTaxes(String tcid, double dValue) {
        TaxInfo tax = this.taxeslogic.getTaxInfo(tcid, this.m_oTicket.getCustomer());
        double dTaxRate = tax == null ? 0.0 : tax.getRate();
        return dValue / (1.0 + dTaxRate);
    }

    private double getInputValue() {
        try {
            return Double.parseDouble(this.m_jPrice.getText());
        }
        catch (NumberFormatException e) {
            return 0.0;
        }
    }

    private double getPorValue() {
        try {
            return Double.parseDouble(this.m_jPor.getText().substring(1));
        }
        catch (NumberFormatException | StringIndexOutOfBoundsException e) {
            return 1.0;
        }
    }

    private void stateToZero() {
        this.m_jPor.setText("");
        this.m_jPrice.setText("");
        this.m_sBarcode = new StringBuffer();
        this.m_iNumberStatus = 0;
        this.m_iNumberStatusInput = 0;
        this.m_iNumberStatusPor = 0;
        this.repaint();
    }

    private void incProductByCode(String sCode) {
        try {
            ProductInfoExt oProduct = this.dlSales.getProductInfoByCode(sCode);
            if (oProduct == null) {
                Toolkit.getDefaultToolkit().beep();
                JOptionPane.showMessageDialog(null, sCode + " - " + AppLocal.getIntString((String)"message.noproduct"), "Check", 2);
                this.stateToZero();
            } else {
                this.incProduct(oProduct);
            }
        }
        catch (BasicException eData) {
            this.stateToZero();
            new MessageInf((Throwable)eData).show((Component)this);
        }
    }

    private void incProductByCodePrice(String sCode, double dPriceSell) {
        try {
            ProductInfoExt oProduct = this.dlSales.getProductInfoByCode(sCode);
            if (oProduct == null) {
                Toolkit.getDefaultToolkit().beep();
                new MessageInf(-33554432, AppLocal.getIntString((String)"message.noproduct")).show((Component)this);
                this.stateToZero();
            } else if (this.m_jaddtax.isSelected()) {
                TaxInfo tax = this.taxeslogic.getTaxInfo(oProduct.getTaxCategoryID(), this.m_oTicket.getCustomer());
                this.addTicketLine(oProduct, 1.0, dPriceSell / (1.0 + tax.getRate()));
            } else {
                this.addTicketLine(oProduct, 1.0, dPriceSell);
            }
        }
        catch (BasicException eData) {
            this.stateToZero();
            new MessageInf((Throwable)eData).show((Component)this);
        }
    }

    private void incProduct(ProductInfoExt prod) {
        if (prod.isScale() && this.m_App.getDeviceScale().existsScale()) {
            try {
                Double value = this.m_App.getDeviceScale().readWeight();
                if (value != null) {
                    this.incProduct(value, prod);
                }
            }
            catch (ScaleException e) {
                Toolkit.getDefaultToolkit().beep();
                new MessageInf(-33554432, AppLocal.getIntString((String)"message.noweight"), (Object)e).show((Component)this);
                this.stateToZero();
            }
        } else if (!prod.isVprice()) {
            this.incProduct(1.0, prod);
        } else {
            Toolkit.getDefaultToolkit().beep();
            JOptionPane.showMessageDialog(null, AppLocal.getIntString((String)"message.novprice"));
        }
    }

    private void incProduct(double dPor, ProductInfoExt prod) {
        if (prod.isVprice()) {
            this.addTicketLine(prod, this.getPorValue(), this.getInputValue());
        } else {
            this.addTicketLine(prod, dPor, prod.getPriceSell());
        }
    }

    protected void buttonTransition(ProductInfoExt prod) {
        if (this.m_iNumberStatusInput == 0 && this.m_iNumberStatusPor == 0) {
            this.incProduct(prod);
        } else if (this.m_iNumberStatusInput == 1 && this.m_iNumberStatusPor == 0) {
            this.incProduct(this.getInputValue(), prod);
        } else if (prod.isVprice()) {
            this.addTicketLine(prod, this.getPorValue(), this.getInputValue());
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
    }

    private void stateTransition(char cTrans) {
        block145 : {
            if (cTrans == '\n' || cTrans == '?') {
                if (this.m_sBarcode.length() > 0) {
                    String sCode = this.m_sBarcode.toString();
                    if (sCode.startsWith("c")) {
                        try {
                            CustomerInfoExt newcustomer = this.dlSales.findCustomerExt(sCode);
                            if (newcustomer == null) {
                                Toolkit.getDefaultToolkit().beep();
                                new MessageInf(-33554432, AppLocal.getIntString((String)"message.nocustomer")).show((Component)this);
                            } else {
                                this.m_oTicket.setCustomer(newcustomer);
                                this.m_jTicketId.setText(this.m_oTicket.getName(this.m_oTicketExt));
                                this.m_oTicket.getSumvoucher(this.dlVouchers, this.jLabel1);
                            }
                        }
                        catch (BasicException e) {
                            Toolkit.getDefaultToolkit().beep();
                            new MessageInf(-33554432, AppLocal.getIntString((String)"message.nocustomer"), (Object)e).show((Component)this);
                        }
                        this.stateToZero();
                    } else if (sCode.startsWith(";")) {
                        this.stateToZero();
                    } else if (sCode.length() == 13 || sCode.length() == 8 || sCode.startsWith("2") || sCode.startsWith("02")) {
                        try {
                            TaxInfo tax;
                            ProductInfoExt oProduct = this.dlSales.getProductInfoByCode(sCode);
                            if (oProduct == null) {
                                Toolkit.getDefaultToolkit().beep();
                                JOptionPane.showMessageDialog(null, sCode + " - " + AppLocal.getIntString((String)"message.noproduct"), "Check", 2);
                                this.stateToZero();
                                break block145;
                            }
                            oProduct.setProperty("product.barcode", sCode);
                            double dPriceSell = oProduct.getPriceSell();
                            double weight = 1.0;
                            String sVariableTypePrefix = sCode.substring(0, 2);
                            String sVariableNum = sCode.length() > 8 ? sCode.substring(8, 12) : null;
                            if (sVariableTypePrefix.equals("20")) {
                                dPriceSell = Double.parseDouble(sVariableNum) / 100.0;
                            } else if (sVariableTypePrefix.equals("21")) {
                                dPriceSell = Double.parseDouble(sVariableNum) / 10.0;
                            } else if (sVariableTypePrefix.equals("22")) {
                                dPriceSell = Double.parseDouble(sVariableNum);
                            } else if (sVariableTypePrefix.equals("23")) {
                                weight = Double.parseDouble(sVariableNum) / 1000.0;
                            } else if (sVariableTypePrefix.equals("24")) {
                                weight = Double.parseDouble(sVariableNum) / 100.0;
                            } else if (sVariableTypePrefix.equals("25")) {
                                weight = Double.parseDouble(sVariableNum) / 10.0;
                            } else if (sVariableTypePrefix.equals("28")) {
                                sVariableNum = sCode.substring(7, 12);
                                dPriceSell = Double.parseDouble(sVariableNum) / 100.0;
                            }
                            if (sVariableTypePrefix.equals("20") || sVariableTypePrefix.equals("21") || sVariableTypePrefix.equals("22")) {
                                tax = this.taxeslogic.getTaxInfo(oProduct.getTaxCategoryID(), this.m_oTicket.getCustomer());
                                oProduct.setProperty("product.price", Double.toString(dPriceSell /= 1.0 + tax.getRate()));
                            } else if (sVariableTypePrefix.equals("23") || sVariableTypePrefix.equals("24") || sVariableTypePrefix.equals("25")) {
                                oProduct.setProperty("product.weight", Double.toString(weight));
                            } else if (sVariableTypePrefix.equals("28")) {
                                tax = this.taxeslogic.getTaxInfo(oProduct.getTaxCategoryID(), this.m_oTicket.getCustomer());
                                oProduct.setProperty("product.price", Double.toString(dPriceSell /= 1.0 + tax.getRate()));
                                weight = -1.0;
                            }
                            if (this.m_jaddtax.isSelected()) {
                                tax = this.taxeslogic.getTaxInfo(oProduct.getTaxCategoryID(), this.m_oTicket.getCustomer());
                                this.addTicketLine(oProduct, weight, dPriceSell / (1.0 + tax.getRate()));
                                break block145;
                            }
                            this.addTicketLine(oProduct, weight, dPriceSell);
                        }
                        catch (BasicException eData) {
                            this.stateToZero();
                            new MessageInf((Throwable)eData).show((Component)this);
                        }
                    } else {
                        this.incProductByCode(sCode);
                    }
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            } else {
                this.m_sBarcode.append(cTrans);
                if (cTrans == '') {
                    this.stateToZero();
                } else if (cTrans == '0' && this.m_iNumberStatus == 0) {
                    this.m_jPrice.setText("0");
                } else if ((cTrans == '1' || cTrans == '2' || cTrans == '3' || cTrans == '4' || cTrans == '5' || cTrans == '6' || cTrans == '7' || cTrans == '8' || cTrans == '9') && this.m_iNumberStatus == 0) {
                    if (!this.priceWith00.booleanValue()) {
                        this.m_jPrice.setText(Character.toString(cTrans));
                    } else {
                        this.m_jPrice.setText(this.setTempjPrice(Character.toString(cTrans)));
                    }
                    this.m_iNumberStatus = 2;
                    this.m_iNumberStatusInput = 1;
                } else if ((cTrans == '0' || cTrans == '1' || cTrans == '2' || cTrans == '3' || cTrans == '4' || cTrans == '5' || cTrans == '6' || cTrans == '7' || cTrans == '8' || cTrans == '9') && this.m_iNumberStatus == 2) {
                    if (!this.priceWith00.booleanValue()) {
                        this.m_jPrice.setText(this.m_jPrice.getText() + cTrans);
                    } else {
                        this.m_jPrice.setText(this.setTempjPrice(this.m_jPrice.getText() + cTrans));
                    }
                } else if (cTrans == '.' && this.m_iNumberStatus == 0 && !this.priceWith00.booleanValue()) {
                    this.m_jPrice.setText("0.");
                    this.m_iNumberStatus = 1;
                } else if (cTrans == '.' && this.m_iNumberStatus == 0) {
                    this.m_jPrice.setText("");
                    this.m_iNumberStatus = 0;
                } else if (cTrans == '.' && this.m_iNumberStatus == 2 && !this.priceWith00.booleanValue()) {
                    this.m_jPrice.setText(this.m_jPrice.getText() + ".");
                    this.m_iNumberStatus = 3;
                } else if (cTrans == '.' && this.m_iNumberStatus == 2) {
                    if (!this.priceWith00.booleanValue()) {
                        this.m_jPrice.setText(this.m_jPrice.getText() + "00");
                    } else {
                        this.m_jPrice.setText(this.setTempjPrice(this.m_jPrice.getText() + "00"));
                    }
                    this.m_iNumberStatus = 2;
                } else if (cTrans == '0' && (this.m_iNumberStatus == 1 || this.m_iNumberStatus == 3)) {
                    if (!this.priceWith00.booleanValue()) {
                        this.m_jPrice.setText(this.m_jPrice.getText() + cTrans);
                    } else {
                        this.m_jPrice.setText(this.setTempjPrice(this.m_jPrice.getText() + cTrans));
                    }
                } else if (!(cTrans != '1' && cTrans != '2' && cTrans != '3' && cTrans != '4' && cTrans != '5' && cTrans != '6' && cTrans != '7' && cTrans != '8' && cTrans != '9' || this.m_iNumberStatus != 1 && this.m_iNumberStatus != 3)) {
                    this.m_jPrice.setText(this.m_jPrice.getText() + cTrans);
                    this.m_iNumberStatus = 3;
                    this.m_iNumberStatusInput = 1;
                } else if (cTrans == '*' && (this.m_iNumberStatus == 2 || this.m_iNumberStatus == 3)) {
                    this.m_jPor.setText("x");
                    this.m_iNumberStatus = 4;
                } else if (cTrans == '*' && (this.m_iNumberStatus == 0 || this.m_iNumberStatus == 1)) {
                    this.m_jPrice.setText("0");
                    this.m_jPor.setText("x");
                    this.m_iNumberStatus = 4;
                } else if (cTrans == '0' && this.m_iNumberStatus == 4) {
                    this.m_jPor.setText("x0");
                } else if ((cTrans == '1' || cTrans == '2' || cTrans == '3' || cTrans == '4' || cTrans == '5' || cTrans == '6' || cTrans == '7' || cTrans == '8' || cTrans == '9') && this.m_iNumberStatus == 4) {
                    this.m_jPor.setText("x" + Character.toString(cTrans));
                    this.m_iNumberStatus = 6;
                    this.m_iNumberStatusPor = 1;
                } else if ((cTrans == '0' || cTrans == '1' || cTrans == '2' || cTrans == '3' || cTrans == '4' || cTrans == '5' || cTrans == '6' || cTrans == '7' || cTrans == '8' || cTrans == '9') && this.m_iNumberStatus == 6) {
                    this.m_jPor.setText(this.m_jPor.getText() + cTrans);
                } else if (cTrans == '.' && this.m_iNumberStatus == 4 && !this.priceWith00.booleanValue()) {
                    this.m_jPor.setText("x0.");
                    this.m_iNumberStatus = 5;
                } else if (cTrans == '.' && this.m_iNumberStatus == 4) {
                    this.m_jPor.setText("x");
                    this.m_iNumberStatus = 1;
                } else if (cTrans == '.' && this.m_iNumberStatus == 6 && !this.priceWith00.booleanValue()) {
                    this.m_jPor.setText(this.m_jPor.getText() + ".");
                    this.m_iNumberStatus = 7;
                } else if (cTrans == '.' && this.m_iNumberStatus == 6) {
                    this.m_jPor.setText(this.m_jPor.getText() + "00");
                    this.m_iNumberStatus = 1;
                } else if (cTrans == '0' && (this.m_iNumberStatus == 5 || this.m_iNumberStatus == 7)) {
                    this.m_jPor.setText(this.m_jPor.getText() + cTrans);
                } else if (!(cTrans != '1' && cTrans != '2' && cTrans != '3' && cTrans != '4' && cTrans != '5' && cTrans != '6' && cTrans != '7' && cTrans != '8' && cTrans != '9' || this.m_iNumberStatus != 5 && this.m_iNumberStatus != 7)) {
                    this.m_jPor.setText(this.m_jPor.getText() + cTrans);
                    this.m_iNumberStatus = 7;
                    this.m_iNumberStatusPor = 1;
                } else if (cTrans == '\u00a7' && this.m_iNumberStatusInput == 1 && this.m_iNumberStatusPor == 0) {
                    if (this.m_App.getDeviceScale().existsScale() && this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
                        try {
                            Double value = this.m_App.getDeviceScale().readWeight();
                            if (value != null) {
                                ProductInfoExt product = this.getInputProduct();
                                this.addTicketLine(product, value, product.getPriceSell());
                            }
                        }
                        catch (ScaleException e) {
                            Toolkit.getDefaultToolkit().beep();
                            new MessageInf(-33554432, AppLocal.getIntString((String)"message.noweight"), (Object)e).show((Component)this);
                            this.stateToZero();
                        }
                    } else {
                        Toolkit.getDefaultToolkit().beep();
                    }
                } else if (cTrans == '\u00a7' && this.m_iNumberStatusInput == 0 && this.m_iNumberStatusPor == 0) {
                    int i = this.m_ticketlines.getSelectedIndex();
                    if (i < 0) {
                        Toolkit.getDefaultToolkit().beep();
                    } else if (this.m_App.getDeviceScale().existsScale()) {
                        try {
                            Double value = this.m_App.getDeviceScale().readWeight();
                            if (value != null) {
                                TicketLineInfo newline = new TicketLineInfo(this.m_oTicket.getLine(i));
                                newline.setMultiply(value.doubleValue());
                                newline.setPrice(Math.abs(newline.getPrice()));
                                this.paintTicketLine(i, newline);
                            }
                        }
                        catch (ScaleException e) {
                            Toolkit.getDefaultToolkit().beep();
                            new MessageInf(-33554432, AppLocal.getIntString((String)"message.noweight"), (Object)e).show((Component)this);
                            this.stateToZero();
                        }
                    } else {
                        Toolkit.getDefaultToolkit().beep();
                    }
                } else if (cTrans == '+' && this.m_iNumberStatusInput == 0 && this.m_iNumberStatusPor == 0) {
                    int i = this.m_ticketlines.getSelectedIndex();
                    if (i < 0) {
                        Toolkit.getDefaultToolkit().beep();
                    } else {
                        TicketLineInfo newline = new TicketLineInfo(this.m_oTicket.getLine(i));
                        if (this.m_oTicket.getTicketType() == 1) {
                            newline.setMultiply(newline.getMultiply() - 1.0);
                            this.paintTicketLine(i, newline);
                        } else {
                            newline.setMultiply(newline.getMultiply() + 1.0);
                            this.paintTicketLine(i, newline);
                        }
                    }
                } else if (cTrans == '-' && this.m_iNumberStatusInput == 0 && this.m_iNumberStatusPor == 0 && this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
                    int i = this.m_ticketlines.getSelectedIndex();
                    if (i < 0) {
                        Toolkit.getDefaultToolkit().beep();
                    } else {
                        TicketLineInfo newline = new TicketLineInfo(this.m_oTicket.getLine(i));
                        if (this.m_oTicket.getTicketType() == 1) {
                            newline.setMultiply(newline.getMultiply() + 1.0);
                            if (newline.getMultiply() >= 0.0) {
                                this.removeTicketLine(i);
                            } else {
                                this.paintTicketLine(i, newline);
                            }
                        } else {
                            newline.setMultiply(newline.getMultiply() - 1.0);
                            if (newline.getMultiply() <= 0.0) {
                                this.removeTicketLine(i);
                            } else {
                                this.paintTicketLine(i, newline);
                            }
                        }
                    }
                } else if (cTrans == '+' && this.m_iNumberStatusInput == 0 && this.m_iNumberStatusPor == 1) {
                    int i = this.m_ticketlines.getSelectedIndex();
                    if (i < 0) {
                        Toolkit.getDefaultToolkit().beep();
                    } else {
                        double dPor = this.getPorValue();
                        TicketLineInfo newline = new TicketLineInfo(this.m_oTicket.getLine(i));
                        if (this.m_oTicket.getTicketType() == 1) {
                            newline.setMultiply(- dPor);
                            newline.setPrice(Math.abs(newline.getPrice()));
                            this.paintTicketLine(i, newline);
                        } else {
                            newline.setMultiply(dPor);
                            newline.setPrice(Math.abs(newline.getPrice()));
                            this.paintTicketLine(i, newline);
                        }
                    }
                } else if (cTrans == '-' && this.m_iNumberStatusInput == 0 && this.m_iNumberStatusPor == 1 && this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
                    int i = this.m_ticketlines.getSelectedIndex();
                    if (i < 0) {
                        Toolkit.getDefaultToolkit().beep();
                    } else {
                        double dPor = this.getPorValue();
                        TicketLineInfo newline = new TicketLineInfo(this.m_oTicket.getLine(i));
                        if (this.m_oTicket.getTicketType() == 0) {
                            newline.setMultiply(dPor);
                            newline.setPrice(- Math.abs(newline.getPrice()));
                            this.paintTicketLine(i, newline);
                        }
                    }
                } else if (cTrans == '+' && this.m_iNumberStatusInput == 1 && this.m_iNumberStatusPor == 0 && this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
                    ProductInfoExt product = this.getInputProduct();
                    this.addTicketLine(product, 1.0, product.getPriceSell());
                    this.m_jEditLine.doClick();
                } else if (cTrans == '-' && this.m_iNumberStatusInput == 1 && this.m_iNumberStatusPor == 0 && this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
                    ProductInfoExt product = this.getInputProduct();
                    this.addTicketLine(product, 1.0, - product.getPriceSell());
                } else if (cTrans == '+' && this.m_iNumberStatusInput == 1 && this.m_iNumberStatusPor == 1 && this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
                    ProductInfoExt product = this.getInputProduct();
                    this.addTicketLine(product, this.getPorValue(), product.getPriceSell());
                } else if (cTrans == '-' && this.m_iNumberStatusInput == 1 && this.m_iNumberStatusPor == 1 && this.m_App.getAppUserView().getUser().hasPermission("sales.EditLines")) {
                    ProductInfoExt product = this.getInputProduct();
                    this.addTicketLine(product, this.getPorValue(), - product.getPriceSell());
                } else if (cTrans == ' ' || cTrans == '=') {
                    if (this.m_oTicket.getLinesCount() > 0) {
                        if (this.closeTicket(this.m_oTicket, this.m_oTicketExt)) {
                            this.m_ticketsbag.deleteTicket();
                            String autoLogoff = this.m_App.getProperties().getProperty("till.autoLogoff");
                            if (autoLogoff != null && autoLogoff.equals("true")) {
                                if ("restaurant".equals(this.m_App.getProperties().getProperty("machine.ticketsbag")) && "true".equals(this.m_App.getProperties().getProperty("till.autoLogoffrestaurant"))) {
                                    this.deactivate();
                                    this.setActiveTicket(null, null);
                                } else {
                                    ((JRootApp)this.m_App).closeAppView();
                                }
                            }
                        } else {
                            this.refreshTicket();
                        }
                    } else {
                        Toolkit.getDefaultToolkit().beep();
                    }
                }
            }
        }
    }

    protected void closelastTicket(TicketInfo ticket, Object ticketext) {
        if (this.m_App.getAppUserView().getUser().hasPermission("sales.Total")) {
            this.printTicket("Printer.Ticket", ticket, this.m_oTicketExt);
        }
    }

    protected boolean closeTicket(TicketInfo ticket, Object ticketext) {
        if (this.listener != null) {
            this.listener.stop();
        }
        boolean resultok = false;
        String printnum1 = this.m_App.getProperties().getProperty("Printer.number");
        int printnum = 1;
        try {
            printnum = Integer.parseInt(StringUtils.isNumeric((String)printnum1) && !printnum1.trim().isEmpty() ? printnum1 : "1");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        if (this.m_App.getAppUserView().getUser().hasPermission("sales.Total")) {
            block28 : {
                this.warrantyCheck(ticket);
                try {
                    JPaymentSelect paymentdialog;
                    block29 : {
                        int i;
                        this.taxeslogic.calculateTaxes(ticket);
                        if (ticket.getTotal() >= 0.0) {
                            ticket.resetPayments();
                        }
                        if (this.executeEvent(ticket, ticketext, "ticket.total", new ScriptArg[0]) != null) break block28;
                        if (this.listener != null) {
                            this.listener.stop();
                        }
                        for (int m = 0; m < printnum; ++m) {
                            this.printTicket("Printer.TicketTotal", ticket, ticketext);
                        }
                        paymentdialog = ticket.getTicketType() == 0 ? this.paymentdialogreceipt : this.paymentdialogrefund;
                        paymentdialog.setPrintSelected("true".equals(this.m_jbtnconfig.getProperty("printselected", "true")));
                        paymentdialog.setTransactionID(ticket.getTransactionID());
                        List deferredTicket = this.dlSales.getDeferredTicketInfoList(ticket.getId());
                        if (!deferredTicket.isEmpty()) {
                            double given = 0.0;
                            for (DeferredTicketInfo dticket : deferredTicket) {
                                given += dticket.getGiven();
                            }
                            if (!paymentdialog.showDialog(ticket.getTotal() - given, ticket.getCustomer())) break block28;
                            ticket.setPayments(paymentdialog.getSelectedPayments());
                            boolean checkTotal = false;
                            if (ticket.getTotal() - given > 0.0) {
                                checkTotal = true;
                            }
                            if (this.executeEvent(ticket, ticketext, "ticket.save", new ScriptArg[0]) != null) break block28;
                            try {
                                if (checkTotal) {
                                    this.dlSales.saveTicketPayments(ticket);
                                }
                                if (!this.dlSales.getVouchersPaymentId().equalsIgnoreCase("") && !this.dlSales.getVouchersPaymentId().isEmpty()) {
                                    this.stackvouchers = paymentdialog.getstackvoucherpayment();
                                    while (!this.stackvouchers.isEmpty()) {
                                        this.voucherpaymentinformation = this.stackvouchers.pop();
                                        this.dlVouchers.insertvoucherspayment(UUID.randomUUID().toString(), this.voucherpaymentinformation.getId(), this.dlSales.getVouchersPaymentId(), this.voucherpaymentinformation.getAmount().doubleValue());
                                    }
                                }
                            }
                            catch (BasicException eData) {
                                MessageInf msg = new MessageInf(-67108864, AppLocal.getIntString((String)"message.nosaveticket"), (Object)eData);
                                System.out.println(eData.getMessage());
                                eData.printStackTrace();
                                msg.show((Component)this);
                            }
                            this.executeEvent(ticket, ticketext, "ticket.close", new ScriptArg("print", (Object)paymentdialog.isPrintSelected()));
                            ticket.setPayments(this.dlSales.loadPayments(ticket.getId()));
                            for (int m2 = 0; m2 < printnum; ++m2) {
                                this.printTicket(paymentdialog.isPrintSelected() || this.warrantyPrint != false ? "Printer.Ticket" : "Printer.Ticket2", ticket, ticketext);
                            }
                            resultok = true;
                            break block28;
                        }
                        if (!paymentdialog.showDialog(ticket.getTotal(), ticket.getCustomer())) break block28;
                        ticket.setPayments(paymentdialog.getSelectedPayments());
                        ticket.setId(UUID.randomUUID().toString());
                        for (i = 0; i < ticket.getLinesCount(); ++i) {
                            ticket.getLine(i).setTicket(ticket.getId());
                        }
                        ticket.setUser(this.m_App.getAppUserView().getUser().getUserInfo());
                        ticket.setActiveCash(this.m_App.getActiveCashIndex());
                        ticket.setDate(new Date());
                        if (this.executeEvent(ticket, ticketext, "ticket.save", new ScriptArg[0]) != null) break block28;
                        this.dlSales.saveTicket(ticket, this.m_App.getInventoryLocation());
                        i = 0;
                        OpencartProductInfoExt prod = null;
                        try {
                            for (TicketLineInfo ticketLine : ticket.getLines()) {
                                prod = this.dlSales.getOpencartProductInfoExt(ticketLine.getProductID());
                                if ("-1".equals(OpencartWebServiceConsumer.editOpencartProduct((String)this.m_App.getProperties().getProperty("ws.security.user"), (String)this.m_App.getProperties().getProperty("ws.security.pass"), (String)prod.getOpencartProductId(), (String)String.valueOf(Math.ceil(ticketLine.getMultiply())), (String)prod.getProductOptionValueId()))) {
                                    throw new BasicException(AppLocal.getIntString((String)"ws.edit.message") + ": " + ticketLine.getProductName());
                                }
                                ++i;
                            }
                        }
                        catch (RemoteException | ServiceException ex) {
                            ex.printStackTrace();
                            if (prod.getOpencartProductId() == null) break block29;
                            this.dlIntegration.deferreUpdate(ticket, i);
                        }
                    }
                    if (!this.dlSales.getVouchersPaymentId().equalsIgnoreCase("") && !this.dlSales.getVouchersPaymentId().isEmpty()) {
                        this.stackvouchers = paymentdialog.getstackvoucherpayment();
                        while (!this.stackvouchers.isEmpty()) {
                            this.voucherpaymentinformation = this.stackvouchers.pop();
                            this.dlVouchers.insertvoucherspayment(UUID.randomUUID().toString(), this.voucherpaymentinformation.getId(), this.dlSales.getVouchersPaymentId(), this.voucherpaymentinformation.getAmount().doubleValue());
                        }
                    }
                    this.executeEvent(ticket, ticketext, "ticket.close", new ScriptArg("print", (Object)paymentdialog.isPrintSelected()));
                    for (int m = 0; m < printnum; ++m) {
                        this.printTicket(paymentdialog.isPrintSelected() || this.warrantyPrint != false ? "Printer.Ticket" : "Printer.Ticket2", ticket, ticketext);
                    }
                    resultok = true;
                    if ("restaurant".equals(this.m_App.getProperties().getProperty("machine.ticketsbag")) && !ticket.getOldTicket()) {
                        this.restDB.clearCustomerNameInTable(ticketext.toString());
                        this.restDB.clearWaiterNameInTable(ticketext.toString());
                        this.restDB.clearTicketIdInTable(ticketext.toString());
                    }
                }
                catch (TaxesException e) {
                    MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotcalculatetaxes"));
                    msg.show((Component)this);
                    resultok = false;
                    e.printStackTrace();
                }
                catch (BasicException ex) {
                    MessageInf msg = new MessageInf(-67108864, AppLocal.getIntString((String)"message.checkdiferred"), (Object)ex);
                    System.out.println(ex.getMessage());
                    ex.printStackTrace();
                    msg.show((Component)this);
                }
            }
            this.m_oTicket.resetTaxes();
            this.m_oTicket.resetPayments();
        }
        return resultok;
    }

    private boolean warrantyCheck(TicketInfo ticket) {
        this.warrantyPrint = false;
        for (int lines = 0; lines < ticket.getLinesCount(); ++lines) {
            if (this.warrantyPrint.booleanValue()) continue;
            this.warrantyPrint = ticket.getLine(lines).isProductWarranty();
            return true;
        }
        return false;
    }

    public String getPickupString(TicketInfo pTicket) {
        if (pTicket == null) {
            return "0";
        }
        String tmpPickupId = Integer.toString(pTicket.getPickupId());
        String pickupSize = this.m_App.getProperties().getProperty("till.pickupsize");
        if (pickupSize != null && Integer.parseInt(pickupSize) >= tmpPickupId.length()) {
            while (tmpPickupId.length() < Integer.parseInt(pickupSize)) {
                tmpPickupId = "0" + tmpPickupId;
            }
        }
        return tmpPickupId;
    }

    protected void printTicket(String sresourcename, TicketInfo ticket, Object ticketext) {
        String sresource = this.dlSystem.getResourceAsXML(sresourcename);
        if (sresource == null) {
            MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotprintticket"));
            msg.show((Component)this);
        } else {
            if (ticket.getPickupId() == 0) {
                try {
                    ticket.setPickupId(this.dlSales.getNextPickupIndex().intValue());
                }
                catch (BasicException e) {
                    ticket.setPickupId(0);
                }
            }
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine((String)"velocity");
                if (Boolean.parseBoolean(this.m_App.getProperties().getProperty("receipt.newlayout"))) {
                    script.put("taxes", (Object)ticket.getTaxLines());
                } else {
                    script.put("taxes", (Object)this.taxcollection);
                }
                script.put("taxeslogic", (Object)this.taxeslogic);
                script.put("ticket", (Object)ticket);
                script.put("place", ticketext);
                script.put("warranty", (Object)this.warrantyPrint);
                script.put("pickupid", (Object)this.getPickupString(ticket));
                this.refreshTicket();
                this.m_TTP.printTicket(script.eval(sresource).toString(), ticket);
            }
            catch (TicketPrinterException | ScriptException e) {
                MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotprintticket"), (Object)e);
                msg.show((Component)this);
            }
        }
    }

    private void printReport(String resourcefile, TicketInfo ticket, Object ticketext) {
        try {
            JasperReport jr;
            InputStream in = this.getClass().getResourceAsStream(resourcefile + ".ser");
            if (in == null) {
                JasperDesign jd = JRXmlLoader.load((InputStream)this.getClass().getResourceAsStream(resourcefile + ".jrxml"));
                jr = JasperCompileManager.compileReport((JasperDesign)jd);
            } else {
                ObjectInputStream oin = new ObjectInputStream(in);
                Throwable throwable = null;
                try {
                    jr = (JasperReport)oin.readObject();
                }
                catch (Throwable var8_12) {
                    throwable = var8_12;
                    throw var8_12;
                }
                finally {
                    if (oin != null) {
                        if (throwable != null) {
                            try {
                                oin.close();
                            }
                            catch (Throwable var8_11) {
                                throwable.addSuppressed(var8_11);
                            }
                        } else {
                            oin.close();
                        }
                    }
                }
            }
            HashMap<String, ResourceBundle> reportparams = new HashMap<String, ResourceBundle>();
            try {
                reportparams.put("REPORT_RESOURCE_BUNDLE", ResourceBundle.getBundle(resourcefile + ".properties"));
            }
            catch (MissingResourceException e) {
                // empty catch block
            }
            reportparams.put("TAXESLOGIC", (ResourceBundle)this.taxeslogic);
            HashMap<String, Object> reportfields = new HashMap<String, Object>();
            reportfields.put("TICKET", (Object)ticket);
            reportfields.put("PLACE", ticketext);
            JasperPrint jp = JasperFillManager.fillReport((JasperReport)jr, reportparams, (JRDataSource)new JRMapArrayDataSource(new Object[]{reportfields}));
            PrintService service = ReportUtils.getPrintService((String)this.m_App.getProperties().getProperty("machine.printername"));
            JRPrinterAWT300.printPages((JasperPrint)jp, (int)0, (int)(jp.getPages().size() - 1), (PrintService)service);
        }
        catch (IOException | ClassNotFoundException | JRException e) {
            MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotloadreport"), e);
            msg.show((Component)this);
        }
    }

    private void visorTicketLine(TicketLineInfo oLine) {
        if (oLine == null) {
            this.m_App.getDeviceTicket().getDeviceDisplay().clearVisor();
        } else {
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine((String)"velocity");
                script.put("ticketline", (Object)oLine);
                this.m_TTP.printTicket(script.eval(this.dlSystem.getResourceAsXML("Printer.TicketLine")).toString());
            }
            catch (TicketPrinterException | ScriptException e) {
                MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotprintline"), (Object)e);
                msg.show((Component)this);
            }
        }
    }

    private /* varargs */ Object evalScript(ScriptObject scr, String resource, ScriptArg ... args) {
        try {
            scr.setSelectedIndex(this.m_ticketlines.getSelectedIndex());
            return scr.evalScript(this.dlSystem.getResourceAsXML(resource), args);
        }
        catch (ScriptException e) {
            MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotexecute"), (Object)e);
            msg.show((Component)this);
            return msg;
        }
    }

    public /* varargs */ void evalScriptAndRefresh(String resource, ScriptArg ... args) {
        if (resource == null) {
            MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotexecute"));
            msg.show((Component)this);
        } else {
            ScriptObject scr = new ScriptObject(this, this.m_oTicket, this.m_oTicketExt, null);
            scr.setSelectedIndex(this.m_ticketlines.getSelectedIndex());
            this.evalScript(scr, resource, args);
            this.refreshTicket();
            this.setSelectedIndex(scr.getSelectedIndex());
        }
    }

    public void printTicket(String resource) {
        this.printTicket(resource, this.m_oTicket, this.m_oTicketExt);
    }

    private /* varargs */ Object executeEventAndRefresh(String eventkey, ScriptArg ... args) {
        String resource = this.m_jbtnconfig.getEvent(eventkey);
        if (resource == null) {
            return null;
        }
        ScriptObject scr = new ScriptObject(this, this.m_oTicket, this.m_oTicketExt, null);
        scr.setSelectedIndex(this.m_ticketlines.getSelectedIndex());
        Object result = this.evalScript(scr, resource, args);
        this.refreshTicket();
        this.setSelectedIndex(scr.getSelectedIndex());
        return result;
    }

    private /* varargs */ Object executeEvent(TicketInfo ticket, Object ticketext, String eventkey, ScriptArg ... args) {
        String resource = this.m_jbtnconfig.getEvent(eventkey);
        if (resource == null) {
            return null;
        }
        ScriptObject scr = new ScriptObject(this, ticket, ticketext, null);
        return this.evalScript(scr, resource, args);
    }

    public String getResourceAsXML(String sresourcename) {
        return this.dlSystem.getResourceAsXML(sresourcename);
    }

    public BufferedImage getResourceAsImage(String sresourcename) {
        return this.dlSystem.getResourceAsImage(sresourcename);
    }

    private void setSelectedIndex(int i) {
        if (i >= 0 && i < this.m_oTicket.getLinesCount()) {
            this.m_ticketlines.setSelectedIndex(i);
        } else if (this.m_oTicket.getLinesCount() > 0) {
            this.m_ticketlines.setSelectedIndex(this.m_oTicket.getLinesCount() - 1);
        }
    }

    private String setTempjPrice(String jPrice) {
        jPrice = jPrice.replace(".", "");
        long tempL = Long.parseLong(jPrice);
        jPrice = Long.toString(tempL);
        while (jPrice.length() < 3) {
            jPrice = "0" + jPrice;
        }
        return jPrice.length() <= 2 ? jPrice : new StringBuffer(jPrice).insert(jPrice.length() - 2, ".").toString();
    }

    private void initComponents() {
        this.m_jPanContainer = new JPanel();
        this.m_jOptions = new JPanel();
        this.m_jButtons = new JPanel();
        this.jButton1 = new JButton();
        this.btnCustomer = new JButton();
        this.btnSplit = new JButton();
        this.m_jPanelScripts = new JPanel();
        this.m_jButtonsExt = new JPanel();
        this.jPanel1 = new JPanel();
        this.m_jbtnScale = new JButton();
        this.jbtnMooring = new JButton();
        this.j_btnKitchenPrt = new JButton();
        this.m_jPanelBag = new JPanel();
        this.m_jPanTicket = new JPanel();
        this.jPanel5 = new JPanel();
        this.jPanel2 = new JPanel();
        this.m_jUp = new JButton();
        this.m_jDown = new JButton();
        this.m_jDelete = new JButton();
        this.m_jList = new JButton();
        this.m_jEditLine = new JButton();
        this.jEditAttributes = new JButton();
        this.m_jPanelCentral = new JPanel();
        this.jPanel4 = new JPanel();
        this.m_jTicketId = new JLabel();
        this.m_jPanTotals = new JPanel();
        this.m_jLblTotalEuros3 = new JLabel();
        this.m_jLblTotalEuros2 = new JLabel();
        this.m_jLblTotalEuros1 = new JLabel();
        this.m_jSubtotalEuros = new JLabel();
        this.m_jTaxesEuros = new JLabel();
        this.m_jTotalEuros = new JLabel();
        this.jLabel1 = new JLabel();
        this.m_jContEntries = new JPanel();
        this.m_jPanEntries = new JPanel();
        this.m_jNumberKeys = new JNumberKeys();
        this.jPanel9 = new JPanel();
        this.m_jPrice = new JLabel();
        this.m_jPor = new JLabel();
        this.m_jEnter = new JButton();
        this.m_jTax = new JComboBox<E>();
        this.m_jaddtax = new JToggleButton();
        this.m_jKeyFactory = new JTextField();
        this.catcontainer = new JPanel();
        this.setBackground(new Color(255, 204, 153));
        this.setLayout(new CardLayout());
        this.m_jPanContainer.setLayout(new BorderLayout());
        this.m_jOptions.setLayout(new BorderLayout());
        this.jButton1.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/customer_add_sml.png")));
        this.jButton1.setToolTipText("Go to Customers");
        this.jButton1.setFocusPainted(false);
        this.jButton1.setFocusable(false);
        this.jButton1.setMargin(new Insets(0, 4, 0, 4));
        this.jButton1.setMaximumSize(new Dimension(50, 40));
        this.jButton1.setMinimumSize(new Dimension(50, 40));
        this.jButton1.setPreferredSize(new Dimension(50, 40));
        this.jButton1.setRequestFocusEnabled(false);
        this.jButton1.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.btnCustomer.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/customer_sml.png")));
        this.btnCustomer.setToolTipText("Find Customers");
        this.btnCustomer.setFocusPainted(false);
        this.btnCustomer.setFocusable(false);
        this.btnCustomer.setMargin(new Insets(0, 4, 0, 4));
        this.btnCustomer.setMaximumSize(new Dimension(50, 40));
        this.btnCustomer.setMinimumSize(new Dimension(50, 40));
        this.btnCustomer.setPreferredSize(new Dimension(50, 40));
        this.btnCustomer.setRequestFocusEnabled(false);
        this.btnCustomer.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.btnSplit.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/sale_split_sml.png")));
        this.btnSplit.setToolTipText("Split Sale");
        this.btnSplit.setFocusPainted(false);
        this.btnSplit.setFocusable(false);
        this.btnSplit.setMargin(new Insets(0, 4, 0, 4));
        this.btnSplit.setMaximumSize(new Dimension(50, 40));
        this.btnSplit.setMinimumSize(new Dimension(50, 40));
        this.btnSplit.setPreferredSize(new Dimension(50, 40));
        this.btnSplit.setRequestFocusEnabled(false);
        this.btnSplit.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        GroupLayout m_jButtonsLayout = new GroupLayout(this.m_jButtons);
        this.m_jButtons.setLayout(m_jButtonsLayout);
        m_jButtonsLayout.setHorizontalGroup(m_jButtonsLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(m_jButtonsLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(this.jButton1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.btnCustomer, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.btnSplit, -2, -1, -2).addGap(5, 5, 5)));
        m_jButtonsLayout.setVerticalGroup(m_jButtonsLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(m_jButtonsLayout.createSequentialGroup().addGap(5, 5, 5).addGroup(m_jButtonsLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jButton1, -2, -1, -2).addComponent(this.btnCustomer, -2, -1, -2).addComponent(this.btnSplit, -2, -1, -2)).addContainerGap()));
        this.m_jOptions.add((Component)this.m_jButtons, "Before");
        this.m_jPanelScripts.setLayout(new BorderLayout());
        this.m_jButtonsExt.setLayout(new BoxLayout(this.m_jButtonsExt, 2));
        this.jPanel1.setMinimumSize(new Dimension(235, 50));
        this.m_jbtnScale.setFont(new Font("Arial", 0, 11));
        this.m_jbtnScale.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/scale.png")));
        this.m_jbtnScale.setText(AppLocal.getIntString((String)"button.scale"));
        this.m_jbtnScale.setToolTipText("Scale");
        this.m_jbtnScale.setFocusPainted(false);
        this.m_jbtnScale.setFocusable(false);
        this.m_jbtnScale.setMargin(new Insets(8, 14, 8, 14));
        this.m_jbtnScale.setMaximumSize(new Dimension(85, 44));
        this.m_jbtnScale.setMinimumSize(new Dimension(85, 44));
        this.m_jbtnScale.setPreferredSize(new Dimension(85, 40));
        this.m_jbtnScale.setRequestFocusEnabled(false);
        this.m_jbtnScale.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel1.add(this.m_jbtnScale);
        this.jbtnMooring.setFont(new Font("Arial", 0, 11));
        ResourceBundle bundle = ResourceBundle.getBundle("pos_messages");
        this.jbtnMooring.setText(bundle.getString("button.moorings"));
        this.jbtnMooring.setMargin(new Insets(8, 14, 8, 14));
        this.jbtnMooring.setMaximumSize(new Dimension(80, 40));
        this.jbtnMooring.setMinimumSize(new Dimension(80, 40));
        this.jbtnMooring.setPreferredSize(new Dimension(80, 40));
        this.jbtnMooring.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel1.add(this.jbtnMooring);
        this.j_btnKitchenPrt.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/printer24.png")));
        this.j_btnKitchenPrt.setText(bundle.getString("button.sendorder"));
        this.j_btnKitchenPrt.setToolTipText("Send to Kichen Printer");
        this.j_btnKitchenPrt.setMargin(new Insets(0, 4, 0, 4));
        this.j_btnKitchenPrt.setMaximumSize(new Dimension(50, 40));
        this.j_btnKitchenPrt.setMinimumSize(new Dimension(50, 40));
        this.j_btnKitchenPrt.setPreferredSize(new Dimension(50, 40));
        this.j_btnKitchenPrt.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel1.add(this.j_btnKitchenPrt);
        this.m_jButtonsExt.add(this.jPanel1);
        this.m_jPanelScripts.add((Component)this.m_jButtonsExt, "After");
        this.m_jOptions.add((Component)this.m_jPanelScripts, "After");
        this.m_jPanelBag.setPreferredSize(new Dimension(0, 50));
        this.m_jPanelBag.setLayout(new BorderLayout());
        this.m_jOptions.add((Component)this.m_jPanelBag, "Center");
        this.m_jPanContainer.add((Component)this.m_jOptions, "North");
        this.m_jPanTicket.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.m_jPanTicket.setLayout(new BorderLayout());
        this.jPanel5.setFont(new Font("Arial", 0, 12));
        this.jPanel5.setPreferredSize(new Dimension(60, 200));
        this.jPanel5.setLayout(new BorderLayout());
        this.jPanel2.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        this.jPanel2.setLayout(new GridLayout(0, 1, 5, 5));
        this.m_jUp.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/1uparrow.png")));
        this.m_jUp.setToolTipText("Scroll Up a Line");
        this.m_jUp.setFocusPainted(false);
        this.m_jUp.setFocusable(false);
        this.m_jUp.setMargin(new Insets(8, 14, 8, 14));
        this.m_jUp.setMaximumSize(new Dimension(42, 36));
        this.m_jUp.setMinimumSize(new Dimension(42, 36));
        this.m_jUp.setPreferredSize(new Dimension(50, 36));
        this.m_jUp.setRequestFocusEnabled(false);
        this.m_jUp.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel2.add(this.m_jUp);
        this.m_jDown.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/1downarrow.png")));
        this.m_jDown.setToolTipText("Scroll Down a Line");
        this.m_jDown.setFocusPainted(false);
        this.m_jDown.setFocusable(false);
        this.m_jDown.setMargin(new Insets(8, 14, 8, 14));
        this.m_jDown.setMaximumSize(new Dimension(42, 36));
        this.m_jDown.setMinimumSize(new Dimension(42, 36));
        this.m_jDown.setPreferredSize(new Dimension(50, 36));
        this.m_jDown.setRequestFocusEnabled(false);
        this.m_jDown.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel2.add(this.m_jDown);
        this.m_jDelete.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/editdelete.png")));
        this.m_jDelete.setToolTipText("Remove Line");
        this.m_jDelete.setFocusPainted(false);
        this.m_jDelete.setFocusable(false);
        this.m_jDelete.setMargin(new Insets(8, 14, 8, 14));
        this.m_jDelete.setMaximumSize(new Dimension(42, 36));
        this.m_jDelete.setMinimumSize(new Dimension(42, 36));
        this.m_jDelete.setPreferredSize(new Dimension(50, 36));
        this.m_jDelete.setRequestFocusEnabled(false);
        this.m_jDelete.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel2.add(this.m_jDelete);
        this.m_jList.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/search32.png")));
        this.m_jList.setToolTipText("Product Search");
        this.m_jList.setFocusPainted(false);
        this.m_jList.setFocusable(false);
        this.m_jList.setMargin(new Insets(8, 14, 8, 14));
        this.m_jList.setMaximumSize(new Dimension(42, 36));
        this.m_jList.setMinimumSize(new Dimension(42, 36));
        this.m_jList.setPreferredSize(new Dimension(50, 36));
        this.m_jList.setRequestFocusEnabled(false);
        this.m_jList.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel2.add(this.m_jList);
        this.m_jEditLine.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/sale_editline.png")));
        this.m_jEditLine.setToolTipText("Edit Line");
        this.m_jEditLine.setFocusPainted(false);
        this.m_jEditLine.setFocusable(false);
        this.m_jEditLine.setMargin(new Insets(8, 14, 8, 14));
        this.m_jEditLine.setMaximumSize(new Dimension(42, 36));
        this.m_jEditLine.setMinimumSize(new Dimension(42, 36));
        this.m_jEditLine.setPreferredSize(new Dimension(50, 36));
        this.m_jEditLine.setRequestFocusEnabled(false);
        this.m_jEditLine.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel2.add(this.m_jEditLine);
        this.jEditAttributes.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/attributes.png")));
        this.jEditAttributes.setToolTipText("Choose Attributes");
        this.jEditAttributes.setFocusPainted(false);
        this.jEditAttributes.setFocusable(false);
        this.jEditAttributes.setMargin(new Insets(8, 14, 8, 14));
        this.jEditAttributes.setMaximumSize(new Dimension(42, 36));
        this.jEditAttributes.setMinimumSize(new Dimension(42, 36));
        this.jEditAttributes.setPreferredSize(new Dimension(50, 36));
        this.jEditAttributes.setRequestFocusEnabled(false);
        this.jEditAttributes.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        this.jPanel2.add(this.jEditAttributes);
        this.jPanel5.add((Component)this.jPanel2, "North");
        this.m_jPanTicket.add((Component)this.jPanel5, "After");
        this.m_jPanelCentral.setFont(new Font("Arial", 0, 14));
        this.m_jPanelCentral.setPreferredSize(new Dimension(450, 240));
        this.m_jPanelCentral.setLayout(new BorderLayout());
        this.jPanel4.setLayout(new BorderLayout());
        this.m_jTicketId.setFont(new Font("Arial", 0, 14));
        this.m_jTicketId.setHorizontalAlignment(2);
        this.m_jTicketId.setVerticalAlignment(1);
        this.m_jTicketId.setAutoscrolls(true);
        this.m_jTicketId.setHorizontalTextPosition(2);
        this.m_jTicketId.setOpaque(true);
        this.m_jTicketId.setPreferredSize(new Dimension(300, 40));
        this.m_jTicketId.setRequestFocusEnabled(false);
        this.m_jTicketId.setVerticalTextPosition(1);
        this.jPanel4.add((Component)this.m_jTicketId, "Center");
        this.m_jPanTotals.setPreferredSize(new Dimension(375, 60));
        this.m_jPanTotals.setLayout(new GridLayout(2, 3, 4, 0));
        this.m_jLblTotalEuros3.setFont(new Font("Arial", 1, 14));
        this.m_jLblTotalEuros3.setHorizontalAlignment(0);
        this.m_jLblTotalEuros3.setLabelFor(this.m_jSubtotalEuros);
        this.m_jLblTotalEuros3.setText(AppLocal.getIntString((String)"label.subtotalcash"));
        this.m_jPanTotals.add(this.m_jLblTotalEuros3);
        this.m_jLblTotalEuros2.setFont(new Font("Arial", 1, 14));
        this.m_jLblTotalEuros2.setHorizontalAlignment(0);
        this.m_jLblTotalEuros2.setLabelFor(this.m_jSubtotalEuros);
        this.m_jLblTotalEuros2.setText(AppLocal.getIntString((String)"label.taxcash"));
        this.m_jPanTotals.add(this.m_jLblTotalEuros2);
        this.m_jLblTotalEuros1.setFont(new Font("Arial", 1, 14));
        this.m_jLblTotalEuros1.setHorizontalAlignment(0);
        this.m_jLblTotalEuros1.setLabelFor(this.m_jTotalEuros);
        this.m_jLblTotalEuros1.setText(AppLocal.getIntString((String)"label.totalcash"));
        this.m_jPanTotals.add(this.m_jLblTotalEuros1);
        this.m_jSubtotalEuros.setBackground(this.m_jEditLine.getBackground());
        this.m_jSubtotalEuros.setFont(new Font("Arial", 0, 18));
        this.m_jSubtotalEuros.setForeground(this.m_jEditLine.getForeground());
        this.m_jSubtotalEuros.setHorizontalAlignment(0);
        this.m_jSubtotalEuros.setLabelFor(this.m_jSubtotalEuros);
        this.m_jSubtotalEuros.setBorder(new LineBorder(new Color(153, 153, 153), 1, true));
        this.m_jSubtotalEuros.setMaximumSize(new Dimension(125, 25));
        this.m_jSubtotalEuros.setMinimumSize(new Dimension(80, 25));
        this.m_jSubtotalEuros.setOpaque(true);
        this.m_jSubtotalEuros.setPreferredSize(new Dimension(80, 25));
        this.m_jSubtotalEuros.setRequestFocusEnabled(false);
        this.m_jPanTotals.add(this.m_jSubtotalEuros);
        this.m_jTaxesEuros.setBackground(this.m_jEditLine.getBackground());
        this.m_jTaxesEuros.setFont(new Font("Arial", 0, 18));
        this.m_jTaxesEuros.setForeground(this.m_jEditLine.getForeground());
        this.m_jTaxesEuros.setHorizontalAlignment(0);
        this.m_jTaxesEuros.setLabelFor(this.m_jTaxesEuros);
        this.m_jTaxesEuros.setBorder(new LineBorder(new Color(153, 153, 153), 1, true));
        this.m_jTaxesEuros.setMaximumSize(new Dimension(125, 25));
        this.m_jTaxesEuros.setMinimumSize(new Dimension(80, 25));
        this.m_jTaxesEuros.setOpaque(true);
        this.m_jTaxesEuros.setPreferredSize(new Dimension(80, 25));
        this.m_jTaxesEuros.setRequestFocusEnabled(false);
        this.m_jPanTotals.add(this.m_jTaxesEuros);
        this.m_jTotalEuros.setBackground(this.m_jEditLine.getBackground());
        this.m_jTotalEuros.setFont(new Font("Arial", 1, 18));
        this.m_jTotalEuros.setForeground(this.m_jEditLine.getForeground());
        this.m_jTotalEuros.setHorizontalAlignment(0);
        this.m_jTotalEuros.setLabelFor(this.m_jTotalEuros);
        this.m_jTotalEuros.setBorder(new LineBorder(new Color(153, 153, 153), 1, true));
        this.m_jTotalEuros.setMaximumSize(new Dimension(125, 25));
        this.m_jTotalEuros.setMinimumSize(new Dimension(80, 25));
        this.m_jTotalEuros.setOpaque(true);
        this.m_jTotalEuros.setPreferredSize(new Dimension(100, 25));
        this.m_jTotalEuros.setRequestFocusEnabled(false);
        this.m_jPanTotals.add(this.m_jTotalEuros);
        this.jPanel4.add((Component)this.m_jPanTotals, "After");
        this.jLabel1.setFont(new Font("Arial", 1, 12));
        this.jLabel1.setForeground(new Color(237, 28, 36));
        this.jLabel1.setText("refund");
        this.jPanel4.add((Component)this.jLabel1, "Last");
        this.m_jPanelCentral.add((Component)this.jPanel4, "South");
        this.m_jPanTicket.add((Component)this.m_jPanelCentral, "Center");
        this.m_jPanContainer.add((Component)this.m_jPanTicket, "Center");
        this.m_jContEntries.setFont(new Font("Arial", 0, 12));
        this.m_jContEntries.setLayout(new BorderLayout());
        this.m_jPanEntries.setLayout(new BoxLayout(this.m_jPanEntries, 1));
        this.m_jNumberKeys.setMinimumSize(new Dimension(200, 200));
        this.m_jNumberKeys.setPreferredSize(new Dimension(250, 250));
        this.m_jNumberKeys.addJNumberEventListener((JNumberEventListener)new /* Unavailable Anonymous Inner Class!! */);
        this.m_jPanEntries.add((Component)this.m_jNumberKeys);
        this.jPanel9.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel9.setLayout(new GridBagLayout());
        this.m_jPrice.setFont(new Font("Arial", 0, 12));
        this.m_jPrice.setHorizontalAlignment(4);
        this.m_jPrice.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(UIManager.getDefaults().getColor("Button.darkShadow")), BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        this.m_jPrice.setOpaque(true);
        this.m_jPrice.setPreferredSize(new Dimension(100, 25));
        this.m_jPrice.setRequestFocusEnabled(false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel9.add((Component)this.m_jPrice, gridBagConstraints);
        this.m_jPor.setFont(new Font("Arial", 0, 12));
        this.m_jPor.setHorizontalAlignment(4);
        this.m_jPor.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(UIManager.getDefaults().getColor("Button.darkShadow")), BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        this.m_jPor.setOpaque(true);
        this.m_jPor.setPreferredSize(new Dimension(22, 25));
        this.m_jPor.setRequestFocusEnabled(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 5, 0, 0);
        this.jPanel9.add((Component)this.m_jPor, gridBagConstraints);
        this.m_jEnter.setIcon(new ImageIcon(this.getClass().getResource("/com/openbravo/images/barcode.png")));
        this.m_jEnter.setToolTipText("Get Barcode");
        this.m_jEnter.setFocusPainted(false);
        this.m_jEnter.setFocusable(false);
        this.m_jEnter.setRequestFocusEnabled(false);
        this.m_jEnter.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 5, 0, 0);
        this.jPanel9.add((Component)this.m_jEnter, gridBagConstraints);
        this.m_jTax.setFont(new Font("Arial", 0, 14));
        this.m_jTax.setFocusable(false);
        this.m_jTax.setPreferredSize(new Dimension(28, 25));
        this.m_jTax.setRequestFocusEnabled(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(5, 0, 0, 0);
        this.jPanel9.add((Component)this.m_jTax, gridBagConstraints);
        this.m_jaddtax.setFont(new Font("Tahoma", 1, 11));
        this.m_jaddtax.setText("+");
        this.m_jaddtax.setFocusPainted(false);
        this.m_jaddtax.setFocusable(false);
        this.m_jaddtax.setPreferredSize(new Dimension(40, 25));
        this.m_jaddtax.setRequestFocusEnabled(false);
        this.m_jaddtax.addActionListener((ActionListener)new /* Unavailable Anonymous Inner Class!! */);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(5, 5, 0, 0);
        this.jPanel9.add((Component)this.m_jaddtax, gridBagConstraints);
        this.m_jPanEntries.add(this.jPanel9);
        this.m_jKeyFactory.setBackground(UIManager.getDefaults().getColor("Panel.background"));
        this.m_jKeyFactory.setForeground(UIManager.getDefaults().getColor("Panel.background"));
        this.m_jKeyFactory.setBorder(null);
        this.m_jKeyFactory.setCaretColor(UIManager.getDefaults().getColor("Panel.background"));
        this.m_jKeyFactory.setPreferredSize(new Dimension(1, 1));
        this.m_jKeyFactory.addKeyListener((KeyListener)new /* Unavailable Anonymous Inner Class!! */);
        this.m_jPanEntries.add(this.m_jKeyFactory);
        this.m_jContEntries.add((Component)this.m_jPanEntries, "North");
        this.m_jPanContainer.add((Component)this.m_jContEntries, "After");
        this.catcontainer.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.catcontainer.setLayout(new BorderLayout());
        this.m_jPanContainer.add((Component)this.catcontainer, "South");
        this.add((Component)this.m_jPanContainer, "ticket");
    }

    private void m_jbtnScaleActionPerformed(ActionEvent evt) {
        this.stateTransition('\u00a7');
    }

    private void m_jEditLineActionPerformed(ActionEvent evt) {
        int i = this.m_ticketlines.getSelectedIndex();
        if (i < 0) {
            Toolkit.getDefaultToolkit().beep();
        } else {
            try {
                TicketLineInfo newline = JProductLineEdit.showMessage((Component)this, (AppView)this.m_App, (TicketLineInfo)this.m_oTicket.getLine(i));
                if (newline != null) {
                    this.paintTicketLine(i, newline);
                }
            }
            catch (BasicException e) {
                new MessageInf((Throwable)e).show((Component)this);
            }
        }
    }

    private void m_jEnterActionPerformed(ActionEvent evt) {
        this.stateTransition('\n');
    }

    private void m_jNumberKeysKeyPerformed(JNumberEvent evt) {
        this.stateTransition(evt.getKey());
    }

    private void m_jKeyFactoryKeyTyped(KeyEvent evt) {
        this.m_jKeyFactory.setText(null);
        this.stateTransition(evt.getKeyChar());
    }

    private void m_jDeleteActionPerformed(ActionEvent evt) {
        int i = this.m_ticketlines.getSelectedIndex();
        if (i < 0) {
            Toolkit.getDefaultToolkit().beep();
        } else {
            this.removeTicketLine(i);
        }
    }

    private void m_jUpActionPerformed(ActionEvent evt) {
        this.m_ticketlines.selectionUp();
    }

    private void m_jDownActionPerformed(ActionEvent evt) {
        this.m_ticketlines.selectionDown();
    }

    private void m_jListActionPerformed(ActionEvent evt) {
        ProductInfoExt prod = JProductFinder.showMessage((Component)this, (DataLogicSales)this.dlSales);
        if (prod != null) {
            this.buttonTransition(prod);
        }
    }

    private void btnCustomerActionPerformed(ActionEvent evt) {
        if (this.listener != null) {
            this.listener.stop();
        }
        JCustomerFinder finder = JCustomerFinder.getCustomerFinder((Component)this, (DataLogicCustomers)this.dlCustomers);
        finder.search((CustomerInfo)this.m_oTicket.getCustomer());
        finder.setVisible(true);
        try {
            if (finder.getSelectedCustomer() == null) {
                this.m_oTicket.setCustomer(null);
            } else {
                this.m_oTicket.setCustomer(this.dlSales.loadCustomerExt(finder.getSelectedCustomer().getId()));
                if ("restaurant".equals(this.m_App.getProperties().getProperty("machine.ticketsbag"))) {
                    this.restDB.setCustomerNameInTableByTicketId(this.dlSales.loadCustomerExt(finder.getSelectedCustomer().getId()).toString(), this.m_oTicket.getId());
                }
            }
        }
        catch (BasicException e) {
            MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotfindcustomer"), (Object)e);
            msg.show((Component)this);
        }
        this.refreshTicket();
    }

    private void btnSplitActionPerformed(ActionEvent evt) {
        if (this.m_oTicket.getLinesCount() > 0) {
            ReceiptSplit splitdialog = ReceiptSplit.getDialog((Component)this, (String)this.dlSystem.getResourceAsXML("Ticket.Line"), (DataLogicSales)this.dlSales, (DataLogicCustomers)this.dlCustomers, (TaxesLogic)this.taxeslogic);
            TicketInfo ticket1 = this.m_oTicket.copyTicket();
            TicketInfo ticket2 = new TicketInfo();
            ticket2.setCustomer(this.m_oTicket.getCustomer());
            if (splitdialog.showDialog(ticket1, ticket2, this.m_oTicketExt) && this.closeTicket(ticket2, this.m_oTicketExt)) {
                this.setActiveTicket(ticket1, this.m_oTicketExt);
            }
        }
    }

    private void jEditAttributesActionPerformed(ActionEvent evt) {
        int i;
        if (this.listener != null) {
            this.listener.stop();
        }
        if ((i = this.m_ticketlines.getSelectedIndex()) < 0) {
            Toolkit.getDefaultToolkit().beep();
        } else {
            try {
                TicketLineInfo line = this.m_oTicket.getLine(i);
                JProductAttEdit attedit = JProductAttEdit.getAttributesEditor((Component)this, (Session)this.m_App.getSession());
                attedit.editAttributes(line.getProductAttSetId(), line.getProductAttSetInstId());
                attedit.setVisible(true);
                if (attedit.isOK()) {
                    line.setProductAttSetInstId(attedit.getAttributeSetInst());
                    line.setProductAttSetInstDesc(attedit.getAttributeSetInstDescription());
                    this.paintTicketLine(i, line);
                }
            }
            catch (BasicException ex) {
                MessageInf msg = new MessageInf(-33554432, AppLocal.getIntString((String)"message.cannotfindattributes"), (Object)ex);
                msg.show((Component)this);
            }
        }
        if (this.listener != null) {
            this.listener.restart();
        }
    }

    private void jButton1ActionPerformed(ActionEvent evt) {
        this.m_App.getAppUserView().showTask("com.openbravo.pos.customers.CustomersPanel");
    }

    private void jbtnMooringActionPerformed(ActionEvent evt) {
        if (this.listener != null) {
            this.listener.stop();
        }
        JMooringDetails mooring = JMooringDetails.getMooringDetails((Component)this, (Session)this.m_App.getSession());
        mooring.setVisible(true);
        if (mooring.isCreate() && mooring.getVesselDays() > 0 && mooring.getVesselSize() > 1) {
            try {
                ProductInfoExt vProduct = this.dlSales.getProductInfoByCode("BFeesDay1");
                vProduct.setName("Berth Fees 1st Day " + mooring.getVesselName());
                this.addTicketLine(vProduct, mooring.getVesselSize().intValue(), vProduct.getPriceSell());
                if (mooring.getVesselDays() > 1) {
                    vProduct = this.dlSales.getProductInfoByCode("BFeesDay2");
                    vProduct.setName("Additional Days " + (mooring.getVesselDays() - 1));
                    this.addTicketLine(vProduct, mooring.getVesselSize() * (mooring.getVesselDays() - 1), vProduct.getPriceSell());
                }
                if (mooring.getVesselPower().booleanValue()) {
                    vProduct = this.dlSales.getProductInfoByCode("PowerSupplied");
                    this.addTicketLine(vProduct, mooring.getVesselDays().intValue(), vProduct.getPriceSell());
                }
            }
            catch (BasicException e) {
                // empty catch block
            }
        }
        this.refreshTicket();
    }

    private void j_btnKitchenPrtActionPerformed(ActionEvent evt) {
        String rScript = this.dlSystem.getResourceAsText("script.SendOrder");
        Interpreter i = new Interpreter();
        try {
            i.set("ticket", (Object)this.m_oTicket);
            i.set("place", this.m_oTicketExt);
            i.set("user", (Object)this.m_App.getAppUserView().getUser());
            i.set("sales", (Object)this);
            i.set("pickupid", this.m_oTicket.getPickupId());
            Object result = i.eval(rScript);
        }
        catch (EvalError ex) {
            Logger.getLogger(JPanelTicket.class.getName()).log(Level.SEVERE, null, (Throwable)ex);
        }
        String autoLogoff = this.m_App.getProperties().getProperty("till.autoLogoff");
        if (autoLogoff != null && autoLogoff.equals("true")) {
            ((JRootApp)this.m_App).closeAppView();
        }
    }

    private void m_jaddtaxActionPerformed(ActionEvent evt) {
        if ("+".equals(this.m_jaddtax.getText())) {
            this.m_jaddtax.setText("-");
        } else {
            this.m_jaddtax.setText("+");
        }
    }

    static /* synthetic */ JTextField access$000(JPanelTicket x0) {
        return x0.m_jKeyFactory;
    }

    static /* synthetic */ int access$200(JPanelTicket x0) {
        return x0.m_iNumberStatusInput;
    }

    static /* synthetic */ int access$300(JPanelTicket x0) {
        return x0.m_iNumberStatusPor;
    }

    static /* synthetic */ double access$400(JPanelTicket x0) {
        return x0.getInputValue();
    }

    static /* synthetic */ void access$500(JPanelTicket x0, String x1, TicketInfo x2, Object x3) {
        x0.printReport(x1, x2, x3);
    }

    static /* synthetic */ ListKeyed access$600(JPanelTicket x0) {
        return x0.taxcollection;
    }

    static /* synthetic */ TaxesLogic access$700(JPanelTicket x0) {
        return x0.taxeslogic;
    }

    static /* synthetic */ JToggleButton access$800(JPanelTicket x0) {
        return x0.m_jaddtax;
    }

    static /* synthetic */ Boolean access$900(JPanelTicket x0) {
        return x0.warrantyPrint;
    }

    static /* synthetic */ void access$1000(JPanelTicket x0, ActionEvent x1) {
        x0.jButton1ActionPerformed(x1);
    }

    static /* synthetic */ void access$1100(JPanelTicket x0, ActionEvent x1) {
        x0.btnCustomerActionPerformed(x1);
    }

    static /* synthetic */ void access$1200(JPanelTicket x0, ActionEvent x1) {
        x0.btnSplitActionPerformed(x1);
    }

    static /* synthetic */ void access$1300(JPanelTicket x0, ActionEvent x1) {
        x0.m_jbtnScaleActionPerformed(x1);
    }

    static /* synthetic */ void access$1400(JPanelTicket x0, ActionEvent x1) {
        x0.jbtnMooringActionPerformed(x1);
    }

    static /* synthetic */ void access$1500(JPanelTicket x0, ActionEvent x1) {
        x0.j_btnKitchenPrtActionPerformed(x1);
    }

    static /* synthetic */ void access$1600(JPanelTicket x0, ActionEvent x1) {
        x0.m_jUpActionPerformed(x1);
    }

    static /* synthetic */ void access$1700(JPanelTicket x0, ActionEvent x1) {
        x0.m_jDownActionPerformed(x1);
    }

    static /* synthetic */ void access$1800(JPanelTicket x0, ActionEvent x1) {
        x0.m_jDeleteActionPerformed(x1);
    }

    static /* synthetic */ void access$1900(JPanelTicket x0, ActionEvent x1) {
        x0.m_jListActionPerformed(x1);
    }

    static /* synthetic */ void access$2000(JPanelTicket x0, ActionEvent x1) {
        x0.m_jEditLineActionPerformed(x1);
    }

    static /* synthetic */ void access$2100(JPanelTicket x0, ActionEvent x1) {
        x0.jEditAttributesActionPerformed(x1);
    }

    static /* synthetic */ void access$2200(JPanelTicket x0, JNumberEvent x1) {
        x0.m_jNumberKeysKeyPerformed(x1);
    }

    static /* synthetic */ void access$2300(JPanelTicket x0, ActionEvent x1) {
        x0.m_jEnterActionPerformed(x1);
    }

    static /* synthetic */ void access$2400(JPanelTicket x0, ActionEvent x1) {
        x0.m_jaddtaxActionPerformed(x1);
    }

    static /* synthetic */ void access$2500(JPanelTicket x0, KeyEvent x1) {
        x0.m_jKeyFactoryKeyTyped(x1);
    }
}
